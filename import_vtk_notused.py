import vtk
import glob
import os
import sys
import numpy as np
# fdir = os.getcwd()
# # check directory
# #if not os.path.exists(fdir):
# #    print('Error: Path does not exist:', fdir)
# #    sys.exit()

# # create reader
# points = vtk.vtkXMLPolyDataReader()
# def print_particlenumber():
# # loop over all vtp files
#     for fname in sorted(glob.glob(os.path.join(fdir, '*.vtp'))):
#         print('Reading:', fname)
#         points.SetFileName(fname)
#         points.Update()

#         # print the arrays
#         data = points.GetOutput()
#         point_data = data.GetPointData()

#         # loop over all data arrays
#         for i in range(point_data.GetNumberOfArrays()):
#             array = point_data.GetArray(i)
#             print(i, point_data.GetArrayName(i), '\t',
#                 'Points:', array.GetNumberOfTuples(),
#                 'Components:', array.GetNumberOfComponents(),
#                 )

fdir = os.getcwd()
# check directory
def print_particlenumber():
# loop over all vtp files
    mb = vtk.vtkMultiBlockDataSet()
    points = vtk.vtkXMLPolyDataReader()
    e = vtk.vtkElevationFilter()
    for fname in sorted(glob.glob(os.path.join(fdir, '*.vtp'))):
        print('Reading:', fname)
        points.SetFileName(fname)
        points.Update()

        # print the arrays
        data = points.GetOutput()
        point_data = data.GetPointData()

        # loop over all data arrays
        for i in range(point_data.GetNumberOfArrays()):
            array = point_data.GetArray(i)
            print(i, point_data.GetArrayName(i), '\t',
                'Points:', array.GetNumberOfTuples(),
                'Components:', array.GetNumberOfComponents(),
                )

        e.SetInputData(mb)
        e.Update()
        vtk_data = e.GetOutputDataObject(0)
        from vtk.numpy_interface import dataset_adapter as dsa
        DAT_Path = os.path.join(fdir, 'particle_input.dat')
        # # vtk_data = inputs[0]
        vtk_dataset_adapter = dsa.WrapDataObject(vtk_data)
        coords = vtk_dataset_adapter.GetPoints()
        data_export = np.column_stack(coords)
        data_export = np.transpose(data_export)
        data_export = np.around(data_export, decimals=2)
        header = "X Y Z"
        np.savetxt(DAT_Path, data_export)
###call the function
print_particlenumber()