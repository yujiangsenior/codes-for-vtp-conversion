#!/bin/bash
dirprefix=2dCFB
originprefix=PARTICLES_0
tailfix=.vtp
newprefix=2D_CFB_case10_
dir_num=1
for dir in $dirprefix*
do
    k=0
    echo $dir
#    \cp -rf ReadPolyData.py $dir
    cd $dir
    for i in $originprefix*$tailfix
    do
      echo "$i"
      echo "$dir"
#    pvbatch pv.py $i
      python ../ReadPolyData.py $i $dir
      newfilename="$newprefix""$dir_num"run_$k.dat
      echo "$newfilename"
      mv particle_input.dat "$newfilename"
      let k++
    done
    let dir_num++
    cd ..
done
