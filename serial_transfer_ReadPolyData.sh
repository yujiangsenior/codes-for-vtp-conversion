#!/bin/bash

#SBATCH -p ProdQ
#SBATCH --nodes=1
#SBATCH --time=02:00:00
#SBATCH -e run.error
#SBATCH -A uceng010c

module purge
module load conda
source activate vtk_error
dirprefix=2dCFB
originprefix=PARTICLES_0
tailfix=.vtp
newprefix=2D_CFB_case10_
dir_num=1
for dir in $dirprefix*
do
    k=0
    echo $dir
#    \cp -rf ReadPolyData.py $dir
    cd $dir
    for i in $originprefix*$tailfix
    do
      echo "$i"
      echo "$dir"
      python ../serial_ReadPolyData.py $i $dir
      newfilename="$newprefix""$dir_num"run_$k.dat
      echo "$newfilename"
      mv particle_input.dat "$newfilename"
      let k++
    done
    let dir_num++
    cd ..
done
