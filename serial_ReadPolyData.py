#!/usr/bin/env python

import vtk
import sys
import os
from vtk.numpy_interface import dataset_adapter as dsa
import numpy as np

# def get_program_parameters():
#     import argparse
#     description = 'Read a polydata file.'
#     epilogue = ''''''
#     parser = argparse.ArgumentParser(description=description, epilog=epilogue,
#                                      formatter_class=argparse.RawDescriptionHelpFormatter)
#     parser.add_argument(dest = 'PARTICLES_0000.vtp', type = argparse.FileType('r'))
#     args = parser.parse_args()
#     print(args.file.readlines())
#     return args.filename


#def main():
colors = vtk.vtkNamedColors()

# filename = get_program_parameters()
filename = sys.argv[1]
#filename, directory = sys.argv[1], sys.argv[2]
#filename = os.path.join(directory, filename)
# Read all the data from the file
reader = vtk.vtkXMLPolyDataReader()
reader.SetFileName(filename)
reader.Update()
# Get the vtk_data
vtk_data = reader.GetOutput()
# Get file path
fdir = os.getcwd()
DAT_Path = os.path.join(fdir, 'particle_input.dat')
# Get positions of points
vtk_dataset_adapter = dsa.WrapDataObject(vtk_data)
coords = vtk_dataset_adapter.GetPoints()
data_export = np.column_stack(coords)
data_export = np.transpose(data_export)
data_export = np.around(data_export, decimals=2)
header = "X Y Z"
# Output
np.savetxt(DAT_Path, data_export)

# # Visualize
# mapper = vtk.vtkPolyDataMapper()
# mapper.SetInputConnection(reader.GetOutputPort())

# actor = vtk.vtkActor()
# actor.SetMapper(mapper)
# actor.GetProperty().SetColor(colors.GetColor3d('NavajoWhite'))

# renderer = vtk.vtkRenderer()
# renderWindow = vtk.vtkRenderWindow()
# renderWindow.AddRenderer(renderer)
# renderWindowInteractor = vtk.vtkRenderWindowInteractor()
# renderWindowInteractor.SetRenderWindow(renderWindow)

# renderer.AddActor(actor)
# renderer.SetBackground(colors.GetColor3d('DarkOliveGreen'))
# renderer.GetActiveCamera().Pitch(90)
# renderer.GetActiveCamera().SetViewUp(0, 0, 1)
# renderer.ResetCamera()

# renderWindow.SetSize(600, 600)
# renderWindow.Render()
# renderWindow.SetWindowName('ReadPolyData')
# renderWindowInteractor.Start()


#if __name__ == '__main__':
#    main()
