In order to run this python script, one need to create a conda environment which includes vtk package.
For instance: 
conda create -n vtk_error python=3.6
conda activate vtk_error
pip install vtk

3 ways to transform .vtp to .dat:
####use transfer_ReadPolyData.sh, (which calls the ReadPolyData.py)
1.in Windows, git bash here，conda init bash
2.conda activate vtk_error
3.sh transfer_ReadPolyData.sh
####use ReadPolyData.py only
1.conda activate vtk_error
2.python ReadPolyData.py PARTICLES_0000.vtp
####use serial_transfer_ReadPolyData.sh, (which calls the serial_ReadPolyData.py)
1.in servers that have slurm to manage the tasks submission: sbatch serial_transfer_ReadPolyData.sh
